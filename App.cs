using System;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using Unity;
using Unity.Lifetime;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Phone;
public class App : AppBase
{
	public App(IAppHost appHost)
		: base(appHost)
	{
		// Register services here to make them available to components via DI, for example:
        // Container.RegisterInstance(new MyServiceAbc())
	}
}
