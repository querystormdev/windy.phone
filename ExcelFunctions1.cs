using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using PhoneNumbers;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.Phone;

public class ExcelFunctions1
{
    PhoneNumberUtil phoneUtil = PhoneNumberUtil.GetInstance();

	[ExcelFunction("Windy.Phone.IsValidNumber")]
	public bool IsValidNumber(string text, string defaultRegion = "US")
	{
		var phoneNumber = phoneUtil.Parse(text, defaultRegion);
		return phoneUtil.IsValidNumber(phoneNumber);
	}

	[ExcelFunction("Windy.Phone.FindNumbers")]
	public IEnumerable<string> FindNumbers(string text, string defaultRegion = "US", PhoneNumberFormat format = PhoneNumberFormat.E164 )
	{
		return phoneUtil.FindNumbers(text, defaultRegion).Select(x => x.Number).Select(pn=>phoneUtil.Format(pn, format));
	}

	[ExcelFunction("Windy.Phone.GetExamplePhoneNumber")]
	public string GetExamplePhoneNumber(string regionCode, PhoneNumberType type = PhoneNumberType.FIXED_LINE, PhoneNumberFormat format = PhoneNumberFormat.E164)
	{
		return phoneUtil.Format(phoneUtil.GetExampleNumberForType(regionCode, type), format);
	}

    [ExcelFunction("Windy.Phone.GetCountryCode")]
    public int GetCountryCode(string internationPhoneNumber)
    {
        var phoneNumber = phoneUtil.Parse(internationPhoneNumber, null);
        
        var x = phoneNumber.CountryCode;
        
        return phoneNumber.CountryCode;
    }

    [ExcelFunction("Windy.Phone.GetCountry")]
    public string GetCountry(string internationPhoneNumber)
    {
        var phoneNumber = phoneUtil.Parse(internationPhoneNumber, null);
        return phoneUtil.GetRegionCodeForCountryCode(phoneNumber.CountryCode);
    }

    [ExcelFunction("Windy.Phone.ToNational")]
    public string ToNational(string internationPhoneNumber)
    {
        var phoneNumber = phoneUtil.Parse(internationPhoneNumber, null);
        return phoneUtil.Format(phoneNumber, PhoneNumbers.PhoneNumberFormat.NATIONAL);
    }

    [ExcelFunction("Windy.Phone.ToInternational")]
    public string ToInternational(string nationPhoneNumber, object countryOrRegionCode = null)
    {
        PhoneNumber phoneNumber = Parse(nationPhoneNumber, countryOrRegionCode);
        return phoneUtil.Format(phoneNumber, PhoneNumbers.PhoneNumberFormat.INTERNATIONAL);
    }

    [ExcelFunction("Windy.Phone.Format")]
    public string Format(string localOrInternationPhoneNumber, object countryOrRegionCode = null, PhoneNumberFormat format = PhoneNumberFormat.RFC3966)
    {
        var phoneNumber = Parse(localOrInternationPhoneNumber, countryOrRegionCode);
        return phoneUtil.Format(phoneNumber, format);
    }

    private PhoneNumber Parse(string internationPhoneNumber, object countryOrRegionCode)
    {
        string regionCode = null;
        if (countryOrRegionCode != null)
        {
            if (countryOrRegionCode is string s)
                regionCode = s;
            else if (countryOrRegionCode is double d)
                regionCode = phoneUtil.GetRegionCodeForCountryCode((int)d);
            else
                throw new ArgumentException("Invalid country/region code");
        }
        var phoneNumber = phoneUtil.Parse(internationPhoneNumber, regionCode);
        return phoneNumber;
    }
}
